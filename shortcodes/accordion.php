<?php
if ( empty( $content ) ) return;
$title = !empty( $title ) ? $title : 'Title';

?>
<div class="accordion">
	<div class="accordion-header">
		<h3 class="accordion-header-title"><?= $title; ?></h3>
	</div>
	<div class="accordion-content">
		<?= apply_filters('the_content', $content ); ?>
	</div>
</div>

// add to shortcodes array
	'accordion' => array(
		'icon' => 'bars',
		'inline' => true,
		'fields' => array(
			'title' => array(
			),
			'content' => array(
				'type' =>'wysiwyg',
			)
		)
	)
	
// js
	$('.accordion').each( function(){
		toggling.init( $(this).children( '.accordion-header'), $(this).children('.accordion-content'), {
			closeOnOffClick: false
		});
	});