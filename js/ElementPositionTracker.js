define( [], function(){
	var $ = jQuery;
	var $window = $(window);
	function ElementPositionTracker($el){
		return {
			$el: $el,
			windowTopOffset: 0,
			windowBottomOffset: 0,
			get top(){
				return this.$el.offset().top;
			},
			get left(){
				return this.$el.offset().left;
			},
			get height(){
				return this.$el.outerHeight();
			},
			get bottom(){
				return this.top + this.height;
			},
			getOffsetFromWindow: function( thisBottomOrTop, windowBottomOrTop, percentOrPixel){
				if ( thisBottomOrTop !== 'bottom' ) thisBottomOrTop = 'top';
				if ( windowBottomOrTop !== 'bottom' ) windowBottomOrTop = 'top';
				if ( percentOrPixel !== '%' ) percentOrPixel = 'px';
				var pos;
				if ( thisBottomOrTop === 'top' ){
					if ( windowBottomOrTop === 'top' ){
						pos = this.top - this.windowTop;
					} else {
						pos = this.windowBottom - this.top;
					}
				} else {
					if ( windowBottomOrTop === 'top' ){
						pos = this.bottom - this.windowTop;
					} else {
						pos = this.windowBottom - this.bottom;
					}
				}
				if ( percentOrPixel === '%' ){
					return pos/(this.windowBottom - this.windowTop);
				} else {
					return pos;
				}
			},
			get windowTop(){
				return $window.scrollTop() + this.windowTopOffset;
			},
			get windowBottom(){
				return $window.scrollTop() + $window.height() - this.windowBottomOffset;
			},
			get inView(){
				var windowTop = this.windowTop;
				var windowBottom = this.windowBottom;

				if ( windowBottom >= this.top && windowBottom <= this.bottom ){
					return true;
				} else if ( windowTop >= this.top && windowTop <= this.bottom ){
					return true;
				} else if ( windowTop < this.top && windowBottom > this.bottom ){
					return true;
				}
				return false;
			}
		}
	}
	return ElementPositionTracker;
});