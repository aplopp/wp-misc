	var tabs = ( function(){
		var init = function($tabsContainers, args){
			var settings = $.extend( {
				tabContainerSelector: '.tabs',
				tabSelector: '.tab',
				panelContainerSelector: '.tab-panels',
				panelSelector: '.tab-panel',
				onSwitch: function(from, to){},
				onAfterSwitch: function(from, to){},
			}, args );
			$tabsContainers.each( function(){
				var $container = $(this);
				var $tabsContainer = $container.find( settings.tabContainerSelector );
				var $tabs = $tabsContainer.find( settings.tabSelector );
				var $panelsContainer = $container.find(settings.panelContainerSelector );
				var $panels = $panelsContainer.find(settings.panelSelector );

				var $activePanel = $();
				var $activeTab = $();
				var speed = 400;
				function activatePanel( id ){
					var $panelToActivate = $panels.filter( '#panel-' + id );
					if ( ! $panelToActivate.size() > 0 ) return;
					var $tabToActivate = $tabs.filter( function(){
						return $(this).children('a').attr('href') === '#'+id
					});
					var callbackArgs = {};
					if ( $activePanel.length > 0 ){
						callbackArgs.from = {
							id: $activeTab.children('a').attr('href').replace( '#', '' ),
							tab: $activeTab,
							panel: $activePanel
						};
					}
					callbackArgs.to = {
						id: $tabToActivate.children('a').attr('href').replace( '#', '' ),
						tab: $tabToActivate,
						panel: $panelToActivate
					};
					settings.onSwitch.apply( $container[0], [ callbackArgs ])
					$panelsContainer.height($activePanel.outerHeight());
					$activePanel.hide();
					$activeTab.removeClass('selected');
					$activePanel = $panelToActivate.css({
						opacity: 0
					}).show().animate({
						opacity: 1
					}, speed, function(){
						settings.onAfterSwitch.apply( $container[0], [ callbackArgs ])
					});
					$activeTab = $tabToActivate;
					$activeTab.addClass('selected');
					$panelsContainer.find('.plan-boxes').trigger('resized-w');
					$panelsContainer.animate({
						height: $activePanel.outerHeight()
					}, speed, function(){
						$panelsContainer.height( '' );
					});
				}
				$window.hashchange( function( e ){
					var id = window.location.hash ? window.location.hash.substr(1) : false;
					if ( id ){
						activatePanel( id );
					}
				});
				$panels.each( function(){
					$(this).attr('id', 'panel-' + $(this).attr('id'));
				});
				$window.hashchange();
				if ( ! $activePanel.size() > 0 ){
					activatePanel( $tabs.first().find( 'a' ).attr('href').substr(1) );
				}
			});
		};
		return {
			init: init
		};
	}() );