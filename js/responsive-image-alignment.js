	var $mainContentContainer = $('#content > article');
	$mainContentContainer.resizedEvent();
	$mainContentContainer.find( 'img.alignleft, img.alignright' ).each( function(){
		var w = $(this).width();
		var originalClass = $(this).hasClass('alignleft') ? 'alignleft' : 'alignright';
		var _isFullWidth = false;
		$mainContentContainer.on('resized-w resized-init', function( e, info ){
			if ( !_isFullWidth ){
				if ( ($mainContentContainer.width() - w ) <= 160 ){
					$(this).removeClass(originalClass).addClass('alignnone');
					_isFullWidth = true;
				}
			} else {
				if ( ($mainContentContainer.width() - w ) >= 160 ){
					$(this).removeClass('alignnone').addClass(originalClass);
					_isFullWidth = false;
				}
			}
		}.bind(this));
	});
