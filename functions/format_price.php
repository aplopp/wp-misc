
function format_price( $price, $with_cents = true, $per = false ){
	if ( strpos( $price, '$') === 0 ){
		$price = substr($price, 1);
	}
	if ( strpos( $price, '*') === strlen( $price ) - 1 ){
		$price = substr( $price, 0, strlen( $price ) - 1 );
		if ( $with_cents ){
			$price = money_format('%i', $price );
		}
		$price .='<sup class="asterisk">*</sup>';
	} else {
		if ( $with_cents ){
			$price = money_format( '%i', $price );
		}
	}
	$price = '<sup class="dollar-sign">$</sup>'.$price;
	if ( $per ){
		$price .= '<sup class="per">'.$per.'</sup>';
	}
	return $price;
}
