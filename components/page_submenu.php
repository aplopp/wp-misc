<?php
if ( $page == 0 ){
	$page = 'top';
} else if( !isset($page) ){
	global $post;
	$page = $post;
} else {
	if ( is_numeric( $page ) ){
		$page = get_post( $page );
	}
}
if ( $page !== 'top' && $page->post_type !== 'page' ) return;

if (empty( $title ) ) $title = '';
if (empty( $parent_as_first ) ) $parent_as_first = false;
if (!empty( $all_children ) ) $depth = 0;

if (!isset( $depth ) ){
	$depth = 1;
} else if ( $depth == 0 ){
	$depth = 100;
}

/*
Used in the page_submenu shortcode, for example
 */
if ( ! function_exists( 'custom_the_menu_item' )){
	function custom_the_menu_item( $page, $menu_items = array(), $depth = 1, $extra_classes = array() ){
		$depth--;
		$classes = array_merge( $extra_classes, array( 'menu-item' ) );
		$url = get_permalink( $page->ID );
		$target = '';
		if ( strpos( $url, '/' ) !== 0 && strpos( $url, get_bloginfo('url') ) !== 0 ){
			// doesn't begin with current domain
			$target = 'target="_blank"';
		} else if ( preg_match( '/\.[a-zA-Z]+$/', $url ) ){
			$target = 'target="_blank"';
		}
		$classes[] = 'page-id-' . $page->ID;
		if ( is_page($page->ID) ) $classes[] = 'current-menu-item'; ?>
		<li class="<?= implode( ' ', $classes ); ?>">
			<a href="<?= $url; ?>" <?= $target; ?> ><?= $page->post_title; ?></a>
			<?php if ( $depth > 0 && !empty( $menu_items[ $page->ID ] ) ){ ?>
			<ul class="sub-menu">
				<?php foreach( $menu_items[ $page->ID ] as $subitem ){
					custom_the_menu_item( $subitem, $menu_items, $depth );
				} ?>
			</ul>
			<?php } ?>
		</li>
		<?php
	}
}

$subpages = get_pages( array(
	'post_type' => 'page',
	'child_of' => ( $page === 'top' ) ? 0 : $page->ID, // all descendants
	'posts_per_page' => -1,
	'sort_column' => 'menu_order',
	'sort_order' => 'asc'
));
if ( !$subpages ) return;

$submenu_items = array();
foreach( $subpages as $subpage ){
	if ( !isset( $submenu_items[ $subpage->post_parent ] ) ){
		$submenu_items[ $subpage->post_parent ] = array();
	}
	$submenu_items[ $subpage->post_parent ][] = $subpage;
}


if ( $title ){ ?>
<h3 class="page-submenu-title"><?= $title; ?></h3>
<?php } ?>
<ul class="page-submenu">
<?php if ( $parent_as_first && $page !== 'top' ){
	custom_the_menu_item( $page, array(), 1, array('parent-menu-item') );
}
$page_id = $page === 'top' ? 0 : $page->ID;
foreach( $submenu_items[ $page_id ] as $item ){
	custom_the_menu_item( $item, $submenu_items, $depth );
} ?>
</ul>