<?php
// add to functions.php
// Fix for wp_mail not sending issue
add_filter( 'wp_mail', function( $email ){
	if ( strpos( $email['to'], '<' ) === false ){
		$email_address = $email['to'];
		$default_email_name = ucfirst( substr( $email_address, 0, strpos( $email_address, '@') ) ); // everything before the @, capitalized, as a default name
		$email['to'] = $default_email_name . ' <'.$email_address .'>';
	}
	return $email;
});
