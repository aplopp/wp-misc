export interface ITogglingSettings {
	openSpeed?: number,
	closeSpeed?: number,
	activeClass?: string,
	effect?: 'slide'|'fade',
	closeOnOffClick?: boolean,
	onOpen?: Function,
	onAfterOpen?: Function,
	onClose?: Function,
	onAfterClose?: Function,
	showFunction?: string,
	hideFunction?: string,
}
export default class Toggling {
	$button: JQuery;
	$item: JQuery;
	_open: boolean;
	settings: ITogglingSettings;
	constructor( $button: JQuery, $item:JQuery, userArgs: ITogglingSettings ){
		this.$button = $button;
		this.$item = $item;
		this._open = false;
		var args: ITogglingSettings = {
			openSpeed: 200,
			closeSpeed: 200,
			activeClass: 'active',
			effect: 'slide',
			closeOnOffClick: true,
			showFunction: 'slideDown',
			hideFunction: 'slideUp',
			onOpen: function(){},
			onAfterOpen: function(){},
			onClose: function(){},
			onAfterClose: function(){}
		};
		this.settings = jQuery.extend( args, userArgs );
		if ( this.settings.effect === 'fade' ){
			this.settings.hideFunction = 'fadeOut';
			this.settings.showFunction = 'fadeIn';
		}
		this.$button.on( 'click', (e) => {
			if ( jQuery(e.target).prop( 'tagName' ) === 'A' &&  jQuery(e.target).attr('href') ){
				return;
			}
			e.preventDefault();
			e.stopPropagation();
			this.toggleItem()
		});

	}
	open(){
		this._open = true;
		this.$item[ this.settings.showFunction ]( this.settings.openSpeed, () => {
			this.settings.onAfterClose.apply( this.$item[0] );
		});
		this.$button.addClass( this.settings.activeClass );
		this.settings.onOpen.apply( this.$item[0] );
		if ( ! this.settings.closeOnOffClick ) return;
		setTimeout( () => {
			jQuery('body').on( 'mousedown.mobile-menu touchstart.mobile-menu', (e) => {
				if ( jQuery(e.target).closest( this.$item.add( this.$button ) ).length === 0 ){
					this.close();
				}
			});
		}, 100 );
	}
	close(){
		this.$button.removeClass( this.settings.activeClass );
		this.settings.onClose.apply( this.$item[0] );
		this.$item[ this.settings.hideFunction ]( this.settings.closeSpeed, () => {
			this._open = false;
			this.settings.onAfterClose.apply( this.$item[0] );
		});
		jQuery(window).unbind( 'mousedown.mobile-menu touchstart.mobile-menu' );
	}
	toggleItem() {
		if( this._open ) {
			this.close();
		} else {
			this.open();
		}
	}
}
