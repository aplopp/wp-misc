export interface IMenuSettings {
	menuSelector?: string,
	topLevelMenuItemSelector?: string,
	activeClass?: string
}
export default class MenuResize {
	public $el: JQuery; // the parent element (typically nav)
	public $menu: JQuery; // the menu element (typically .menu)
	public settings: IMenuSettings;
	protected fullMenuEnabled: boolean;
	protected isFullMenuVisible: boolean;
	public constructor( $el: JQuery, args: IMenuSettings = {} ){
		this.$el = $el;
		this.settings = jQuery.extend({}, {
			menuSelector: '.menu',
			topLevelMenuItemSelector: '.menu > li',
			activeClass: 'show-full-menu'
		}, args);
		this.$menu = this.$el.find( this.settings.menuSelector );
		var _timeout;
		jQuery(window).on('resize', () => {
			clearTimeout( _timeout)
			_timeout = setTimeout( () => {
				this.handleResize()
			}, 50);
		});
		this.handleResize();
	}
	public handleResize(){
		this.$el.addClass(this.settings.activeClass);
		var menuWidth = 0;
		this.$el.find( this.settings.topLevelMenuItemSelector ).each( (i, el) => {
			menuWidth += jQuery(el).outerWidth();
			menuWidth += 5; // for space
		});
		var availableWidth = this.$menu.width();
		var nextFullMenuEnabled = menuWidth < availableWidth;
		if ( nextFullMenuEnabled === this.fullMenuEnabled ){
			// no change
			if ( !this.fullMenuEnabled ){
				this.$el.removeClass( this.settings.activeClass );
			}
			return;
		}
		this.fullMenuEnabled = nextFullMenuEnabled;
		if ( this.fullMenuEnabled ){
			this.showFullMenu();
		} else {
			this.hideFullMenu();
		}
	}
	public toggleMenu(){
		if ( !this.fullMenuEnabled ){
			if ( this.isFullMenuVisible ){
				this.hideFullMenu();
			} else {
				this.showFullMenu();
			}
		}
	}
	showFullMenu(){
		this.isFullMenuVisible = true;
		this.$el.addClass(this.settings.activeClass)
		jQuery(window).trigger('full-menu-show');
	}
	hideFullMenu(){
		this.isFullMenuVisible = false;
		this.$el.removeClass(this.settings.activeClass)
		jQuery(window).trigger('full-menu-hide');
	}
}