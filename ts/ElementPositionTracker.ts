interface IElementPositionTracker {
	$el: JQuery,
	windowTopOffset: number,
	windowBottomOffset: number,
	top: number,
	left: number,
	height: number,
	bottom: number,
	getAmountVisible(percentOrPixel?: '%'|'px'): number,
	getOffsetFromWindow(thisBottomOrTop?: 'top'|'bottom', windowBottomOrTop?: 'top'|'bottom', percentOrPixel?: '%'|'px'),
	windowTop: number,
	windowBottom: number,
	inView: boolean
}
function ElementPositionTracker($el, args = {}){
	var settings = jQuery.extend({}, {
		windowTopOffset: 0,
		windowBottomOffset: 0
	}, args);
	var $window = jQuery(window);
	var myTracker: IElementPositionTracker = {
		$el: $el,
		// these can be set at any time, directly
		windowTopOffset: settings.windowTopOffset,
		windowBottomOffset: settings.windowBottomOffset,
		get top(){
			return this.$el.offset().top;
		},
		get left(){
			return this.$el.offset().left;
		},
		get height(){
			return this.$el.outerHeight();
		},
		get bottom(){
			return this.top + this.height;
		},
		getAmountVisible: function(percentOrPixel){
			if ( percentOrPixel !== '%' ) percentOrPixel = 'px';
			if ( ! this.inView ){
				return 0;
			}
			var windowTop = this.windowTop;
			var windowBottom = this.windowBottom;

			var amount;
			if ( windowBottom >= this.top && windowBottom <= this.bottom ){
				amount = windowBottom - this.top;
			} else if ( windowTop >= this.top && windowTop <= this.bottom ){
				amount = this.bottom - windowTop
			} else if ( windowTop < this.top && windowBottom > this.bottom ){
				amount = this.height;
			}
			return percentOrPixel === '%' ? amount/this.height : amount;
		},
		getOffsetFromWindow: function( thisBottomOrTop, windowBottomOrTop, percentOrPixel){
			if ( thisBottomOrTop !== 'bottom' ) thisBottomOrTop = 'top';
			if ( windowBottomOrTop !== 'bottom' ) windowBottomOrTop = 'top';
			if ( percentOrPixel !== '%' ) percentOrPixel = 'px';
			var pos;
			if ( thisBottomOrTop === 'top' ){
				if ( windowBottomOrTop === 'top' ){
					pos = this.top - this.windowTop;
				} else {
					pos = this.windowBottom - this.top;
				}
			} else {
				if ( windowBottomOrTop === 'top' ){
					pos = this.bottom - this.windowTop;
				} else {
					pos = this.windowBottom - this.bottom;
				}
			}
			if ( percentOrPixel === '%' ){
				return pos/(this.windowBottom - this.windowTop);
			} else {
				return pos;
			}
		},
		get windowTop(){
			return $window.scrollTop() + this.windowTopOffset;
		},
		get windowBottom(){
			return $window.scrollTop() + $window.height() - this.windowBottomOffset;
		},
		get inView(){
			var windowTop = this.windowTop;
			var windowBottom = this.windowBottom;

			if ( windowTop > this.bottom ){
				return false;
			} else if ( windowBottom < this.top){
				return false;
			}

			if ( windowBottom >= this.top && windowBottom <= this.bottom ){
				return true;
			} else if ( windowTop >= this.top && windowTop <= this.bottom ){
				return true;
			} else if ( windowTop < this.top && windowBottom > this.bottom ){
				return true;
			}
			return false;
		}
	}
	return myTracker;
}
